Niagahoster Knowledge Test

# Niagahoster Knowledge Test

## Test Number 2
Bixboll installation using alpine image. Build 1 image and use 3 base image. 
Service running:
1. Nginx as webserver
2. Php as app
3. Mysql 5 as database

### Requirement

1. Docker 1.19
2. docker-compose

### Instalation
1. Run compose
```bash
$ docker-compose up --build -d
```
2. Open browser and open `localhost:8004\index\install.php` for boxblling and `localhost:8001` for landingpage
3. Follow step by step and waiting for success info after finish installation wizard
